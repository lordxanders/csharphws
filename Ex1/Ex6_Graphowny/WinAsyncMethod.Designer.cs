﻿namespace Ex6_Graphowny {
	partial class WinAsyncMethod {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.helpProvider1 = new System.Windows.Forms.HelpProvider();
			this.button3 = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// button1
			// 
			this.helpProvider1.SetHelpString(this.button1, "Sum these numbers");
			this.button1.Location = new System.Drawing.Point(12, 64);
			this.button1.Name = "button1";
			this.helpProvider1.SetShowHelp(this.button1, true);
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Sum";
			this.toolTip1.SetToolTip(this.button1, "Sum these numbers");
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.helpProvider1.SetHelpString(this.button2, "Show it works");
			this.button2.Location = new System.Drawing.Point(160, 107);
			this.button2.Name = "button2";
			this.helpProvider1.SetShowHelp(this.button2, true);
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Work";
			this.toolTip1.SetToolTip(this.button2, "Show it works");
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox1
			// 
			this.helpProvider1.SetHelpString(this.textBox1, "For input integer A");
			this.textBox1.Location = new System.Drawing.Point(53, 28);
			this.textBox1.Name = "textBox1";
			this.helpProvider1.SetShowHelp(this.textBox1, true);
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 2;
			this.toolTip1.SetToolTip(this.textBox1, "For input integer A");
			// 
			// textBox2
			// 
			this.helpProvider1.SetHelpString(this.textBox2, "For input integer B");
			this.textBox2.Location = new System.Drawing.Point(251, 28);
			this.textBox2.Name = "textBox2";
			this.helpProvider1.SetShowHelp(this.textBox2, true);
			this.textBox2.Size = new System.Drawing.Size(100, 20);
			this.textBox2.TabIndex = 3;
			this.toolTip1.SetToolTip(this.textBox2, "For input integer B");
			// 
			// helpProvider1
			// 
			this.helpProvider1.HelpNamespace = "L:\\Projects\\git2015\\CsharpHWs\\Ex1\\Ex4\\README.txt";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(311, 106);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 4;
			this.button3.Text = "Show Help";
			this.toolTip1.SetToolTip(this.button3, "Show the \'Help\' file");
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// WinAsyncMethod
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 161);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.HelpButton = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "WinAsyncMethod";
			this.Text = "WinAsyncMethod";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.HelpProvider helpProvider1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.ToolTip toolTip1;
	}
}