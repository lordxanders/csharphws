﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex6_Graphowny {
	static class Program {
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			//new BALLS().Show();
			//new WindowsFormChart().Show();
			new WinBackgroundWorker().Show();
			new WinAsyncMethod().Show();
			Application.Run(new Form1());
		}
	}
}
