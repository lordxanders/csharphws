﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex6_Graphowny {
	public partial class WinAsyncMethod : Form
	{
		private delegate int AsyncSum(int a, int b);

		private int Sum(int a, int b)
		{
			System.Threading.Thread.Sleep(9000);
			return a + b;
		}
		public WinAsyncMethod() {
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e) {
			int a, b;
			try
			{
				a = int.Parse(textBox1.Text);
				b = int.Parse(textBox2.Text);
			}
			catch (Exception ex)
			{
				MessageBox.Show("something went wrong");
				textBox1.Text = textBox2.Text = "";
				return;
			}
			AsyncSum sumDelegate = new AsyncSum(Sum);
			AsyncCallback cb = new AsyncCallback(CallBackMethod);
			sumDelegate.BeginInvoke(a, b, cb, sumDelegate);
		}

		private void CallBackMethod(IAsyncResult ar)
		{
			string str;
			AsyncSum sumDelegate = (AsyncSum) ar.AsyncState;
			str = String.Format("Sum equals {0}", sumDelegate.EndInvoke(ar));
			MessageBox.Show(str);
		}

		private void button2_Click(object sender, EventArgs e) {
			MessageBox.Show("ITS ALIVE");
		}

		private void button3_Click(object sender, EventArgs e) {
			Help.ShowHelp(this,helpProvider1.HelpNamespace);
		}
	}
}
