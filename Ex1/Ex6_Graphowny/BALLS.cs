﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex6_Graphowny {
	public partial class BALLS : Form {

		Point location  = new Point(50,50);
		Point speed  = new Point(5,5);

		public BALLS() {
			InitializeComponent();
		}

		private void Go()
		{
			location = new Point(location.X + speed.X, location.Y +speed.Y);
			if (location.X + 40 > ClientRectangle.Width || location.X < 0)
			{
				speed = new Point(-speed.X, speed.Y);
			}
			if (location.Y + 40 > ClientRectangle.Height || location.Y < 0)
			{
				speed = new Point(speed.X,-speed.Y);
			}
		}

		private void Ball()
		{
			Graphics gr = Graphics.FromHwnd(this.Handle);
			gr.FillEllipse(Brushes.Cyan, location.X, location.Y, 40, 40);
			Go();
		}

		private void timer1_Tick(object sender, EventArgs e) {
			Refresh();
			Ball();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e) {
			if (checkBox1.Checked)
			{
				timer1.Enabled = true;
			}
			else
			{
				timer1.Enabled = false;
			}
		}
	}
}
