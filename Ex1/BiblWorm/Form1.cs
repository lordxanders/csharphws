﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiblWorm
{
	public partial class Form1 : Form
	{
		List<Item> its = new List<Item>();

		public string Author
		{
			get { return textBox1.Text; }
			set { textBox1.Text = value; }
		}

		public string Title
		{
			get { return textBox2.Text; }
			set { textBox2.Text = value; }
		}

		public string Publisher
		{
			get { return textBox3.Text; }
			set { textBox3.Text = value; }
		}

		public int Pages
		{
			get { return (int) numericUpDown1.Value; }
			set { numericUpDown1.Value = value; }
		}

		public int Year
		{
			get { return (int) numericUpDown2.Value; }
			set { numericUpDown2.Value = value; }
		}

		public int ID
		{
			get { return (int) numericUpDown3.Value; }
			set { numericUpDown3.Value = value; }
		}

		public int PeriodUse
		{
			get { return (int) numericUpDown4.Value; }
			set { numericUpDown4.Value = value; }
		}

		public bool Existence
		{
			get { return checkBox1.Checked; }
			set { checkBox1.Checked = true; }
		}

		public bool SortID
		{
			get { return checkBox3.Checked; }
			set { checkBox3.Checked = true; }
		}

		public bool ReturnTime
		{
			get { return checkBox2.Checked; }
			set { checkBox2.Checked = true; }
		}

		public Form1()
		{
			InitializeComponent();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
		}

		private void label1_Click(object sender, EventArgs e)
		{
		}

		private void button1_Click(object sender, EventArgs e) {
			Book b = new Book(Author, Title,Publisher, Pages,Year, ID, Existence);
			if (ReturnTime)
				b.ReturnSrok();
			b.PriceBook(PeriodUse);
			its.Add(b);
			Author = Publisher = Title = "";
			Pages = ID = PeriodUse = 0;
			Year = 2000;
			Existence = ReturnTime = false;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (SortID)
				its.Sort();
			StringBuilder sb = new StringBuilder();
			foreach (Item item in its)
			{
				sb.Append("\n" + item.ToString());
			}
			richTextBox1.Text = sb.ToString();
		}
	}
}