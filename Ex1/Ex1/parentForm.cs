﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex1 {
	public partial class parentForm : Form
	{

		private int openDocs = 0;

		public parentForm() {
			InitializeComponent();
			spData.Text = Convert.ToString(System.DateTime.Today.ToLongDateString());
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
			this.Close();
		}

		private void cascadeToolStripMenuItem_Click(object sender, EventArgs e) {
			this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
		}

		private void tileToolStripMenuItem_Click(object sender, EventArgs e) {
			this.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e) {
			ChildForm newChild = new ChildForm();
			newChild.MdiParent = this;
			newChild.Text = newChild.Text + " " + ++openDocs;
			newChild.richTextBox1.Text = "Hi there " + openDocs;
			newChild.Show();
		}

		private void newBrainPowerToolStripMenuItem_Click(object sender, EventArgs e) {
			Romb romb = new Romb();
			romb.MdiParent = this;
			romb.Show();
		}

		private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
			switch (e.ClickedItem.Tag.ToString())
			{
				case "NewDoc":
					ChildForm newChild = new ChildForm();
					newChild.MdiParent = this;
					newChild.Text = newChild.Text + " " + ++openDocs;
					newChild.richTextBox1.Text = "Hi there " + openDocs;
					newChild.Show();
					break;
				case "Cascade":
					this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
					spWin.Text = "Windows is cascade";
					break;
				case "Title":
					this.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
					spWin.Text = "Windows are tiled";
					break;
			}
		}

		private void toolStripStatusLabel2_Click(object sender, EventArgs e) {

		}
	}
}
