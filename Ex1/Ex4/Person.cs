﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Ex4 {
	[Serializable,
		XmlRoot(Namespace = "http://lightless.hol.es")]
	public class Person {
		public string FirstName { get; set; }

		public string LastName { get; set; }

		[NonSerialized]
		private int age;

		[XmlIgnore]
		public int Age
		{
			get { return age; }
			set { age = value; }
		}

		public override string ToString()
		{
			return LastName + " " + FirstName + "\nAge: " + Age + "\n";
		}
	}
}
