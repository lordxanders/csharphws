﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Ex4 {
	public partial class EditPerson : Form {

		List<Person> pers = new List<Person>();


		public EditPerson() {
			InitializeComponent();
			listView1.Invalidate();

		}

		private void button1_Click(object sender, EventArgs e) {
			Person p = new Person();
			EditPersonForm ep = new EditPersonForm(p);
			if (ep.ShowDialog() != DialogResult.OK)
				return;
			pers.Add(p);
			listView1.VirtualListSize = pers.Count;
			listView1.Invalidate();
		}

		private void button2_Click(object sender, EventArgs e) {
			if (listView1.SelectedIndices.Count ==0)
				return;
			Person p = pers[listView1.SelectedIndices[0]];
			//ListViewItem item = listView1.SelectedItems[0];
			EditPersonForm ep = new EditPersonForm(p);
			if (ep.ShowDialog() == DialogResult.OK)
				listView1.Invalidate();
			//ep.FirstName = item.Text;
			//ep.LastName = item.SubItems[1].Text;
			//ep.Age = Convert.ToInt32(item.SubItems[2].Text);
			//if (ep.ShowDialog() != DialogResult.OK)
			//	return;
			//item.Text = ep.FirstName;
			//item.SubItems[1].Text = ep.LastName;
			//item.SubItems[2].Text = ep.Age.ToString();
		}

		private void listView1_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e) {
			if (e.ItemIndex >= 0 && e.ItemIndex < pers.Count)
			{
				e.Item  = new ListViewItem(pers[e.ItemIndex].FirstName);
				e.Item.SubItems.Add(pers[e.ItemIndex].LastName);
				e.Item.SubItems.Add(pers[e.ItemIndex].Age.ToString());
				
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			StringBuilder sb = new StringBuilder();
			foreach (Person item in pers)
			{
				sb.Append("Employee:\n" + item.ToString());
			}
			richTextBox1.Text = sb.ToString();
		}

		private void EditPerson_FormClosing(object sender, FormClosingEventArgs e)
		{
			BinaryFormatter binFormat = new BinaryFormatter();
			using (FileStream fStream = new FileStream("AllMyPersons.dat",
				FileMode.Create, FileAccess.Write, FileShare.None))
			{
				binFormat.Serialize(fStream, pers);
			}
			using (FileStream fStreamXml = new FileStream("PersonCollection.xml",
				FileMode.Create, FileAccess.Write, FileShare.None))
			{
				XmlSerializer xmlFormat = new XmlSerializer(typeof(List<Person>));
				xmlFormat.Serialize(fStreamXml,pers);
			}
		}

		private void EditPerson_Load(object sender, EventArgs e) {
			BinaryFormatter binFormat = new BinaryFormatter();
			try
			{
				using (FileStream fStream = new FileStream("AllMyPersons.dat",
				FileMode.OpenOrCreate, FileAccess.Read, FileShare.None)) {
					pers.AddRange((List<Person>)binFormat.Deserialize(fStream));
					listView1.VirtualListSize = pers.Count;
					listView1.Invalidate();
				}
			}
			catch
			{
			}
		}
	}
}
