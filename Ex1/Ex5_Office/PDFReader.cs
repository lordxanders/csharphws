﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex5_Office {
	public partial class PDFReader : Form {
		public PDFReader() {
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e) {
			this.Hide();
			WinAPIClass.AnimateWindow(this, 3000,
				WinAPIClass.AnimateWindowFlags.AW_ACTIVATE | WinAPIClass.AnimateWindowFlags.AW_BLEND);
			this.button1.Invalidate();
			this.button2.Invalidate();
			this.button3.Invalidate();

		}

		private void button2_Click(object sender, EventArgs e) {
			this.Hide();
			WinAPIClass.AnimateWindow(this, 3000,
				WinAPIClass.AnimateWindowFlags.AW_HOR_POSITIVE | WinAPIClass.AnimateWindowFlags.AW_SLIDE);
			this.button1.Invalidate();
			this.button2.Invalidate();
			this.button3.Invalidate();
		}

		private void button3_Click(object sender, EventArgs e) {
			this.Hide();
			WinAPIClass.AnimateWindow(this, 3000,
				WinAPIClass.AnimateWindowFlags.AW_CENTER | WinAPIClass.AnimateWindowFlags.AW_SLIDE);
			this.button1.Invalidate();
			this.button2.Invalidate();
			this.button3.Invalidate();
		}
	}
}
