﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using ExcelObj = Microsoft.Office.Interop.Excel;

namespace Ex5_Office {
	public partial class Form1 : Form
	{
		private ExcelObj.Application app = new ExcelObj.Application();
		private ExcelObj.Workbook workbook;
		private ExcelObj.Worksheet NwSheet;
		private ExcelObj.Range ShtRange;
		private DataTable dt = new DataTable();

		public Form1() {
			InitializeComponent();
		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {

		}

		private void button1_Click(object sender, EventArgs e) {
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				textBox1.Text = ofd.FileName;
				workbook = app.Workbooks.Open(ofd.FileName);
				NwSheet = (ExcelObj.Worksheet) workbook.Sheets.get_Item(1);
				ShtRange = NwSheet.UsedRange;
				MessageBox.Show("Range of columns: " + ShtRange.Columns.Count);
				for (int Cnum = 1; Cnum <= ShtRange.Columns.Count; Cnum++)
				{
					dt.Columns.Add(new DataColumn((ShtRange.Cells[1, Cnum] as ExcelObj.Range).Value2.ToString()));
				}
				dt.AcceptChanges();
				string[] columnNames = new String[dt.Columns.Count];
				for (int i = 0; i < dt.Columns.Count; i++)
				{
					columnNames[0] = dt.Columns[i].ColumnName; //WTF? with index
				}
				for (int Rnum = 2; Rnum <= ShtRange.Rows.Count; Rnum++)
				{
					DataRow dr = dt.NewRow();
					for (int Cnum = 1; Cnum <= ShtRange.Columns.Count; Cnum++)
					{
						if ((ShtRange.Cells[Rnum, Cnum] as ExcelObj.Range).Value2 != null)
						{
							dr[Cnum - 1] = (ShtRange.Cells[Rnum, Cnum] as ExcelObj.Range).Value2.ToString();
						}
					}
					dt.Rows.Add(dr);
					dt.AcceptChanges();
				}
				dataGridView1.DataSource = dt;
				app.Quit();
			}
			else
			{
				MessageBox.Show("Something went wrong!", "Loading data...", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
