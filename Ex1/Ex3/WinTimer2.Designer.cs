﻿namespace Ex3 {
	partial class WinTimer2 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.clickButton1 = new Ex3.ClickButton();
			this.userControlTimer21 = new Ex3.UserControlTimer2();
			this.SuspendLayout();
			// 
			// clickButton1
			// 
			this.clickButton1.Location = new System.Drawing.Point(67, 141);
			this.clickButton1.Name = "clickButton1";
			this.clickButton1.Size = new System.Drawing.Size(168, 90);
			this.clickButton1.TabIndex = 1;
			this.clickButton1.Text = "clickButton1";
			this.clickButton1.UseVisualStyleBackColor = true;
			// 
			// userControlTimer21
			// 
			this.userControlTimer21.Location = new System.Drawing.Point(84, 38);
			this.userControlTimer21.Name = "userControlTimer21";
			this.userControlTimer21.Size = new System.Drawing.Size(75, 23);
			this.userControlTimer21.TabIndex = 0;
			this.userControlTimer21.Text = "userControlTimer21";
			// 
			// WinTimer2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.clickButton1);
			this.Controls.Add(this.userControlTimer21);
			this.Name = "WinTimer2";
			this.Text = "WinTimer2";
			this.ResumeLayout(false);

		}

		#endregion

		private UserControlTimer2 userControlTimer21;
		private ClickButton clickButton1;
	}
}