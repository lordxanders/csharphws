﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex3
{
	class ClickButton : System.Windows.Forms.Button
	{
		private int mClicks;

		public int Clicks
		{
			get { return mClicks; }
		}

		protected override void OnClick(EventArgs e)
		{
			mClicks++;
			base.OnClick(e);
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			base.OnPaint(pe);
			Graphics g = pe.Graphics;
			SizeF stringSize;
			stringSize = g.MeasureString(Clicks.ToString(), this.Font, this.Width);
			g.DrawString(Clicks.ToString(), this.Font,System.Drawing.SystemBrushes.ControlText,
				this.Width-stringSize.Width-3,this.Height-stringSize.Height-3);
		}
	}
}